{* vim: set ts=2 sw=2 sts=2 et: *}

{**
 * Product details Weight main block
 *
 * @ListChild (list="product.details.common.product-attributes.elements", weight="100")

*}
{if:!isWeightHidden()}
    <li IF="!getWeight()=0" class="product-weight">
      <div><strong>{t(#Weight#)}</strong></div>
      <span>{formatWeight(getWeight())}</span>
    </li>
{end:}
